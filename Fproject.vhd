--Kwaku Kessey-Ankomah Jr

LIBRARY ieee;
use ieee.std_logic_1164.all;
-- Converts the 50 Mhz Altera Clock to a slow clock signal
-- and puts signal out on the GPIO connector ( GPIO_1(1) )

entity Fproject is
	port( LEDG : out std_logic_vector(3 downto 0); -- vector for LEDS
	SW : IN std_LOGIC_VECTOR(4 downto 0);
	GPIO_1 : out std_logic_vector(1 downto 1);
	KEY : IN std_LOGIC_VECTOR(3 downto 0);
	LEDR: out std_LOGIC_VECTOR (7 DOWNTO 4);
	clock_50 : in std_logic); -- Clock variable on DE2 Board
end Fproject;

architecture behavior of Fproject is
	signal Clock: std_logic:= '0'; -- Signal variable for clock output and initialize it to 0
	
	component flipFlop is
		port(D,C: in std_LOGIC;
				Q: OUT std_LOGIC);
	end component;
	
	SIGNAL Qff: std_LOGIC_VECTOR(3 Downto 0);
	SIGNAl NotQff: std_LOGIC_VECTOR(3 Downto 0);
	
	component setPassword is
		PORT ( PIN: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		En: IN STD_LOGIC;
		PINout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
	end component;
	SIGNAL L: std_LOGIC;
	SIGNAL PassPIN: std_logic_vector(3 Downto 0);
begin
	
	LEDR<=PassPIN;
	Pass: setPassword port map (SW(3 downto 0),KEY(0),PassPIN);--set password
	
	


	--LEDG(0) <= Qff;
	--this is the series of Flip Flop Gates connected together
	Ff0: flipFlop port map (NOT(Qff(3)), Clock, Qff(0));
	Ff1: flipFlop port map (Qff(0), Clock, Qff(1) );
	Ff2: flipFlop port map(Qff(1), Clock, Qff(2));
	Ff3: flipFlop port map(Qff(2), Clock, Qff(3));
	
	process(clock_50)
		variable cnt : integer range 0 to 100000000; -- 0-50Mhz counter
		begin
		if(clock_50'event and clock_50='1') then -- on the rising edge of the clock...
			if(cnt=10000000)then -- if freq = .5 Hz
				cnt:=0; -- reset counter to zero
				--ledg(0) <= Clock; -- Assign two LEDs to clock value (to demonstrate)
				--ledg(1) <= Clock;
				Clock <= not Clock; -- Toggle clock
				
				
				
				

			else
				cnt := cnt+1; -- for every other 50Mhz clock cycle increment cnt by 1
			end if;
		end if;
	end process;
	
	
	GPIO_1(1) <= Clock;
end behavior; 

-------------------This is clock
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY clockhertz IS
	PORT ( CLOCK_50 : IN STD_LOGIC; --CLOCK_50 is a signal on the Cyclone II board
	LEDR : BUFFER STD_LOGIC_VECTOR(0 DOWNTO 0);
	GPIO_0 : OUT STD_LOGIC_VECTOR(1 DOWNTO 1));
END clockhertz;

ARCHITECTURE Behavior OF clockhertz IS -- this process divides the clock by 25 million
	SIGNAL count :INTEGER RANGE 0 to 25000000;
BEGIN
	process (CLOCK_50)
	BEGIN
	IF (CLOCK_50'Event AND CLOCK_50 = '1') THEN
	count <= count + 1;
	END IF;
	IF (count = 25000000) THEN
	count <= 0;
	LEDR <= NOT LEDR;
	END IF;
	END process;
	GPIO_0(1) <= CLOCK_50; -- this puts the on-board clock signal out to the header
END Behavior;
-----------------------------------FLipFlop
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY flipFlop IS
	PORT ( D, C: IN STD_LOGIC;
	Q : OUT STD_LOGIC);
END flipFlop;

ARCHITECTURE Behavior OF flipFlop IS -- this is a D flip Flop
		
BEGIN
	process(C)
		BEGIN
			IF C'EVENT AND C ='1' THEN
				Q<=D;
			END IF;
	END PROcess;

END Behavior;
-----------------------------------DLatch
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY DLatchGate IS
	PORT ( D, C: IN STD_LOGIC;
	Q : OUT STD_LOGIC);
END DLatchGate;

ARCHITECTURE Behavior OF DLatchGate IS -- this is a D Latch
		
BEGIN
	process(C)
		BEGIN
			IF C ='1' THEN
				Q<=D;
			END IF;
	END PROcess;
END Behavior;
----------------------------InputCircuittoSetPassword
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY setPassword IS
	PORT ( PIN: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	En: IN STD_LOGIC;
	PINout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END setPassword;

ARCHITECTURE Behavior OF setPassword IS -- this is a D flip Flop

	component DLatchGate is
		port(D,C: in std_LOGIC;
				Q: OUT std_LOGIC);
	end component;
		
	SIGNAL Enable: std_LOGIC;
BEGIN
	--C is the enabler
	--PINout is the saved values
	Enable <= En;
	Dlatch1: DLatchGate port map(PIN(0),En,PINout(0));
	Dlatch2: DLatchGate port map(PIN(1),En,PINout(1));
	Dlatch3: DLatchGate port map(PIN(2),En,PINout(2));
	Dlatch4: DLatchGate port map(PIN(3),En,PINout(3));
	
	
END Behavior;
---------------------------Pin comparision Checker
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY comparePassword IS
	PORT ( PIN: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	Attempt: STD_LOGIC_VECTOR(3 DOWNTO 0);
	isSame : OUT STD_LOGIC);
END comparePassword;

ARCHITECTURE Behavior OF comparePassword IS -- this is a comparision if the two passwords are the same the output is a 1

	SIGNAL Temp: std_LOGIC_Vector(3 Downto 0);
BEGIN
	--C is the enabler
	--PINout is the saved values
	Temp(0) <= NOT(PIN(0) XOR Attempt(0));
	Temp(1) <= NOT(PIN(1) XOR Attempt(1));
	Temp(2) <= NOT(PIN(2) XOR Attempt(2));
	Temp(3) <= NOT(PIN(3) XOR Attempt(3));
	
	isSame <= Temp(0) AND Temp(1) AND Temp(2) AND Temp(3);
	
	
	
END Behavior;
------------------------------------D Flip Flop with reset asychronous 
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY Dflipflop IS
	PORT ( D, Resetn, Clock : IN STD_LOGIC ;
	Q : OUT STD_LOGIC) ;
END Dflipflop ;

ARCHITECTURE Behavior OF flipflop IS

BEGIN
	PROCESS (Resetn, Clock)
	BEGIN
		IF Resetn = ’0’ THEN
			Q <= ’0’;
		ELSIF Clock’EVENT AND Clock = ’1’ THEN
			Q <= D;
		END IF;
	END PROCESS;
END Behavior;
--------------------------------------Flip FLop Circuit Finite State Machine
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY Dflipflop IS
	PORT ( w : IN STD_LOGIC;
	R: IN STD_LOGIC;
	Z : OUT STD_LOGIC_VECTOR(1 Downto 0) ;
END Dflipflop ;

ARCHITECTURE Behavior OF flipflop IS
	SIGNAL Y: STD_LOGIC_VECTOR(1 Downto 0);
	SIGNAL NotYs: STD_LOGIC_VECTOR(1 Downto 0);
	SIGNAL Ys: STD_LOGIC_VECTOR(1 Downto 0);
	SIGNAL Temp: STD_LOGIC_VECTOR(5 Downto 0);
BEGIN
	NotYs(1) <= NOT(Ys(1));
	NotYs(0) <= NOT(Ys(0));
	Y(1) <= w;
	Y(0) <= NOT(w) AND NotY(1) AND NotY(0);
	
	DFf1: Dflipflop port map(Y(1),R,C,Ys(1));
	DFf2: Dflipflop port map(Y(0),R,C,Ys(0));
	
	Z(1) <= Ys(1) AND NotYs(0);
	Z(0) <= NotYs(1) AND Y(0);
	
	
END Behavior;